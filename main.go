package main

import (
	"encoding/json"
	uuid "github.com/satori/go.uuid"
	"github.com/tealeg/xlsx"
	"log"
	"net/http"
	"github.com/go-chi/chi"
)

func getXLSXFile (w http.ResponseWriter, r *http.Request) {

	excelFileName  := chi.URLParam(r, "filename")
	xlsxFile, err := xlsx.OpenFile(excelFileName)
	if err != nil {
		log.Println("cannot open xlsx: ", err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	err = xlsxFile.Write(w)
	if err != nil {
		log.Println("write error: ", err)
	}

}

func createXLSXFile (w http.ResponseWriter, r *http.Request)  {

	uuid, err := uuid.NewV1()
	if err != nil {
		log.Println("cannot generate uuid: ", err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	jsonDoc := map[string]string{}
	err = json.NewDecoder(r.Body).Decode(&jsonDoc)
	if err != nil {
		log.Println("cannot decode json: ", err)
		http.Error(w, "can't read your data", http.StatusBadRequest)
		return
	}

	xlsxFile := xlsx.NewFile()
	sheet, err := xlsxFile.AddSheet("Sheet1")
	if err != nil {
		log.Println("cannot add sheet: ", err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	row := sheet.AddRow()
	for key := range jsonDoc {
		cell := row.AddCell()
		cell.Value = key
	}

	row = sheet.AddRow()
	for key := range jsonDoc {
		cell := row.AddCell()
		cell.Value = jsonDoc[key]
	}

	err = xlsxFile.Save(uuid.String()+".xlsx")
	if err != nil {
		log.Println("cannot save xlsx: ", err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	err = xlsxFile.Write(w)
	if err != nil {
		log.Println("write error: ", err)
	}
}

func main() {
	r := chi.NewRouter()

	r.Get("/{filename}", getXLSXFile)
	r.Post("/", createXLSXFile)

	http.ListenAndServe(":3000", r)
}